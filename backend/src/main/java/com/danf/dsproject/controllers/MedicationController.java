package com.danf.dsproject.controllers;

import com.danf.dsproject.dto.MedUserDTO;
import com.danf.dsproject.dto.MedicationDTO;
import com.danf.dsproject.entities.MedUser;
import com.danf.dsproject.entities.Medication;
import com.danf.dsproject.service.MedUserManagementService;
import com.danf.dsproject.service.MedicationManagementService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequiredArgsConstructor
public class MedicationController {

    private final MedicationManagementService medicationManagementService;


    @GetMapping("/meds")
    public List<Medication> readAll() {
        return medicationManagementService.findAll();
    }

    private Medication medicationDTOtoMedication(MedicationDTO dto) {
        return new Medication(
                dto.getName(),
                dto.getSide_effects());
    }

    @PostMapping("/meds")
    public MedicationDTO create(@RequestBody MedicationDTO dto) {
        return dto.ofEntity(medicationManagementService.save(medicationDTOtoMedication(dto)));
    }


    @PutMapping("/meds")
    public void update (@RequestBody MedicationDTO dto) {
        Optional<Medication> med = medicationManagementService.findMedicationById(dto.getMid());
        if (med.isPresent()) {
            med.get().setName(dto.getName());
            med.get().setSide_effects(dto.getSide_effects());
            medicationManagementService.save(med.get());
        }
    }

    @DeleteMapping("/meds/{id}")
    public String delete (@PathVariable("id") Integer id) {
        Optional<Medication> user = medicationManagementService.findMedicationById(id);
        medicationManagementService.delete(user.get());
        return "Haha yes";
    }

}
