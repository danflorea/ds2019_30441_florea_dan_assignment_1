package com.danf.dsproject.dto;

import com.danf.dsproject.entities.MedUser;
import com.danf.dsproject.entities.MedicationPlan;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompleteMedUserDTO {

    private Integer uid;
    private String name;
    private String gender;
    private String birthdate;
    private String address;
    private String username;
    private String password;
    private String role;
    private List<MedUser> caregivers;
    private List<MedUser> patients;
    private List<MedicationPlan> medicationPlan;

    public CompleteMedUserDTO(MedUser user) {
        CompleteMedUserDTO medUserDTO = new CompleteMedUserDTO();
//        medUserDTO.setUid(user.getUid());
        medUserDTO.setName(user.getName());
        medUserDTO.setGender(user.getGender());
        medUserDTO.setAddress(user.getAddress());
        medUserDTO.setBirthdate(user.getBirthdate());
        medUserDTO.setUsername(user.getUsername());
        medUserDTO.setPassword(user.getPassword());
        medUserDTO.setRole(user.getRole());
        medUserDTO.setCaregivers(user.getCaregivers());
        medUserDTO.setPatients(user.getPatients());
        medUserDTO.setMedicationPlan(user.getMedicationPlan());
    }
}
