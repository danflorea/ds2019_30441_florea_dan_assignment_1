package com.danf.dsproject.dto;

import com.danf.dsproject.entities.MedUser;
import lombok.Data;

@Data
public class MedUserDTO {

    private Integer uid;
    private String name;
    private String gender;
    private String birthdate;
    private String address;
    private String username;
    private String password;
    private String role;


    public static MedUserDTO ofEntity(MedUser user) {
        MedUserDTO medUserDTO = new MedUserDTO();
        medUserDTO.setUid(user.getUid());
        medUserDTO.setName(user.getName());
        medUserDTO.setGender(user.getGender());
        medUserDTO.setAddress(user.getAddress());
        medUserDTO.setBirthdate(user.getBirthdate());
        medUserDTO.setUsername(user.getUsername());
        medUserDTO.setPassword(user.getPassword());
        medUserDTO.setRole(user.getRole());
        return medUserDTO;
    }
}
