package com.danf.dsproject.persistence.api;

import com.danf.dsproject.persistence.jpa.JpaMedUserRepository;

public interface RepositoryFactory {

    MedicationPlanRepository createMedicationPlanRepository();
    MedicationRepository createMedicationRepository();
    JpaMedUserRepository createMedUserRepository();

}
