package com.danf.dsproject.persistence.jpa;

import com.danf.dsproject.entities.MedUser;
import com.danf.dsproject.persistence.api.MedUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.Repository;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

public interface JpaMedUserRepository extends Repository<MedUser, Integer>{
    void delete(MedUser user);

    default void remove(MedUser user) {
        delete(user);
    }

    List<MedUser> findAll();

    MedUser save(MedUser medUser);

    Optional<MedUser> findById (Integer id);

    List<MedUser> findMedUsersByUsername (String username);

    List<MedUser> findMedUsersByCaregivers (List<MedUser> user);
}
