import { Component, OnInit } from '@angular/core';
import { CurrentUser } from 'src/app/models/currentUser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.page.html',
  styleUrls: ['./patient.page.scss'],
})
export class PatientPage implements OnInit {
  isLoggedIn: boolean = false;

  constructor(private router: Router) { }

  ngOnInit() {
    if (CurrentUser.getInstance().uid > 0) {
      this.isLoggedIn = true;
    }

    if (!this.isLoggedIn || CurrentUser.getInstance().role !== 'DOCTOR') {
      this.router.navigate(['/login']);
    }
  }

}
