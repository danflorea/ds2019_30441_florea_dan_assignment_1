import { Component, OnInit } from '@angular/core';
import { CurrentUser } from 'src/app/models/currentUser';
import { Router } from '@angular/router';
import { ConfigService } from 'src/app/services/config/config.service';
import { MedUser } from 'src/app/models/medUser';

@Component({
  selector: 'app-caregiver',
  templateUrl: './caregiver.page.html',
  styleUrls: ['./caregiver.page.scss'],
})
export class CaregiverPage implements OnInit {
  isLoggedIn: boolean = false;
  patients: Array<any> = [];

  constructor(
    private router: Router,
    private configService: ConfigService) { }

  ngOnInit() {
    if (CurrentUser.getInstance().role === "CAREGIVER") {
      this.isLoggedIn = true;
    }else
    {
      this.router.navigate(['/login']);
    }

    // if (!this.isLoggedIn || CurrentUser.getInstance().role !== 'CAREGIVER') {
    //   this.router.navigate(['/login']);
    // }

    this.configService.getConfig("user/17").subscribe(u => {
      this.patients = u;
    })
  }



}
