import { Component } from '@angular/core';
import { Observable, interval, throwError, of } from 'rxjs'
import { HttpClient } from '@angular/common/http';
import { Network } from '@ionic-native/network/ngx'
import { ConfigService } from '../services/config/config.service';
import { Citadel } from './citadel';
import { Product } from './products';
import { retryWhen, flatMap } from 'rxjs/operators';
import { NetworkStatusService } from '../services/network/network-status.service';
import { ServerStatusService } from '../services/network/server-status.service';
import { MedUser } from '../models/medUser';
import { ReducedUserInfo } from '../models/reducedUserInfo';
import { CurrentUser } from '../models/currentUser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})

export class Tab1Page {	

	constructor(
		private configService: ConfigService, 
		private router: Router, 
		private serverStatusService: ServerStatusService) {}
	
	backend = "localhost:8080";
	url = "users";
	usersList: Array<MedUser> = [];
	frm = [];
	isLoggedIn = false;

	nameInput: string;
	addressInput: string;
	usernameInput: string;
	roleInput: string;
	birthdateInput: string;
	genderInput: string;

	uUid: number;
	uName: string;
	uAddress: string;
	uUsername: string;
	uRole: string;
	uBirthdate: string;
	uGender: string;

	displayUpdateForm = true;

	private async delay(ms: number) {
		return new Promise(resolve => setTimeout(() => {
			resolve('delayed');
		}, ms));
	}

	updateUsersList() {
		this.configService.getConfig(this.url).subscribe(u => {
			this.usersList = u;
		});
	}

	ngOnInit() {
		if (CurrentUser.getInstance().role === "DOCTOR") {
			this.isLoggedIn = true;
		} else {
			this.router.navigate(['/login']);
		}
		this.updateUsersList();
	}

	mapToUser(obj): MedUser {
		let u = new MedUser(
			
			obj.name,
			obj.birthdate,
			obj.address,
			obj.username,
			"changeme",
			obj.role,
			obj.gender
		);
		return u;
	}

	deleteUser(medUser:MedUser) 
	{
	   	this.updateUsersList();
		console.log(medUser);
		let r = new ReducedUserInfo(medUser.uid, medUser.username, medUser.password);
		this.configService.deleteConfig(medUser.uid, this.url).subscribe();
		this.updateUsersList();
		console.log(this.serverStatusService.status);
  	}

   async addUser() 
   {
	   let u = new MedUser(
		   this.nameInput, 
		   this.birthdateInput, 
		   this.addressInput, 
		   this.usernameInput, 
		   "pass", 
		   this.roleInput,
		   this.genderInput
		);
		this.configService.postConfig(u, this.url).subscribe(mu => {
			console.log("added user ", mu);
		});
		this.usersList.push(u);
		this.updateUsersList();
   }

   updateUserCardInitializer(user) {
	   	console.log(user);
		this.uName = user.name;
		this.uUsername = user.username;
		this.uRole = user.role;
		this.uBirthdate = user.birthdate;
		this.uAddress = user.address;
		this.uGender = user.gender;
		this.uUid = user.uid;
		document.getElementById("updateUserCard").scrollIntoView();
   }

   updateUser() {
	   let pass = this.uBirthdate[0] + this.uBirthdate[1] + this.uBirthdate[3] + this.uBirthdate[4];
	   let newUser = new MedUser (
		   this.uName,
		   this.uBirthdate,
		   this.uAddress,
		   this.uUsername,
		   pass,
		   this.uRole,
		   this.uGender
	   )
	   newUser.uid = this.uUid;
	   console.log("user", newUser);
	   this.configService.putConfig(newUser, this.url).subscribe();
	   this.updateUsersList();
   }

   
}
