import { Injectable } from '@angular/core';
import { HttpRequest } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs';
import { ConfigService } from '../config/config.service';
import { MedUser } from 'src/app/models/medUser';

@Injectable({
  providedIn: 'root'
})
export class ServerStatusService {

    public status = 'unknown';
    cachedRequests: Array<HttpRequest<any>> = [];
    probing: boolean = false;
    cachedProducts = [];
	probingRequest: Observable<any> = null;
	interval:any;

  
  constructor(private configService: ConfigService, private storage: Storage) { }

  public collectFailedRequest(req: HttpRequest<any>): void {
    this.cachedRequests.push(req);
}

    public retryFailedRequests(): void 
    { 
        
    }



    private async delay(ms: number) {
        return new Promise (resolve => setTimeout(()=>{
            resolve('delayed');
        }, ms));
    }

    public async retryFailedRequestsAsync() 
    {
        
    }

    public storeUnsentItems (medUser: MedUser)
    {
		this.cachedProducts.push(medUser);
    }

    public async storeUnsentItemsAsync (medUser: MedUser)
    {
		this.cachedProducts.push(medUser);
    }
  
    public getStatus(): void {
        this.configService.getConfig("users").subscribe(us => {  });
    }
	
	stopPinging(){
		clearInterval(this.interval);
  }
  
  public async updateStatusAsync() {
    return this.configService.getConfig("users").subscribe(us => { });
  }
  
  
}
