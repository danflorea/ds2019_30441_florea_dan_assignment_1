import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfigService } from 'src/app/services/config/config.service';
import { CurrentUser } from 'src/app/models/currentUser';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(private router: Router, private configService: ConfigService) { }

  iName: string = '';
  iPass: string = '';

  attemptLogin()
  {
    this.configService.getConfig('users').subscribe(u => {
      u.forEach(element => {
        if(element.password === this.iPass && element.username === this.iName)
        {
          CurrentUser.getInstance().initiateCurrentUser(element);
          if (element.role === 'DOCTOR')
          {
            this.router.navigate(['/tabs/tab1']);
          }
          else if (element.role === 'PATIENT')
          {
            this.router.navigate(['/patient']);
          }else if (element.role === 'CAREGIVER')
          {
            this.router.navigate(['/caregiver']);
          }
          CurrentUser.getInstance().initiateCurrentUser(element);
        }
      });
    });
  }

  ngOnInit() {
  }

}
