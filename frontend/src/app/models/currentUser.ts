import { MedUser } from './medUser';

 
 export class CurrentUser {

     private static instance: CurrentUser;
    static getInstance() {
        if (!CurrentUser.instance) {
            CurrentUser.instance = new CurrentUser();
        }
        return CurrentUser.instance;
    }

    public uid: number;
    public name: string;
    public gender: string;
    public address: string;
    public birthdate: string;
    public username: string;
    public password: string;
    public role: string;

    public initiateCurrentUser(user: MedUser)
    {
        this.uid = user.uid;
        this.name = user.name;
        this.birthdate = user.birthdate;
        this.address = user.address;
        this.username = user.username;
        this.password = user.password;
        this.role = user.role;
        this.gender = user.gender;
    }

 }
 