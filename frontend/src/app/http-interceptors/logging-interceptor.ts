import { finalize, tap, catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpResponse, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ServerStatusService } from '../services/network/server-status.service';

@Injectable()
export class LoggingInterceptor implements HttpInterceptor {
	
	constructor (private statusService: ServerStatusService) {}

	intercept (req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> 
	{
		const started = Date.now();
		let ok: string;
		return next.handle(req).pipe(
			tap(
				event => {
                    // this.statusService.status = event instanceof HttpResponse ? 'available' : 'unavailable'; //this is be a tad bit problematic innit
                    if (event instanceof HttpResponse)
                    {
                        this.statusService.status = 'available';
                    }
				},
				error => {
					if(error instanceof HttpErrorResponse)
					{
						this.statusService.status = 'unavailable';
					}
				}
			),
			finalize (async () => {
				// const elapsed = Date.now() - started;
				// const msg = req.method.toString() + " - " + req.urlWithParams.toString() + " " + this.statusService.status + " in " + elapsed + " ms.";
				// console.log("    - Logging interceptor: ", this.statusService.status);
				
				if (req.body != null)
				{
					console.log("Logging interceptor data: ", req.body);
				}
			})
		)
	}
}