import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { Tab1PageModule } from './tab1/tab1.module';
import { TabsPageModule } from './tabs/tabs.module';
import { TabsPage } from './tabs/tabs.page';
import { LoginFormModule } from './components/login-form/login-form.module';

const routes: Routes = [
  {
    path: '', redirectTo: '/login', pathMatch: 'full'
    // loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  { path: 'login', loadChildren: './auth/login/login.module#LoginPageModule' },
  { path: 'caregiver', loadChildren: './components/caregiver/caregiver.module#CaregiverPageModule' },
  { path: 'patient', loadChildren: './components/patient/patient.module#PatientPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
